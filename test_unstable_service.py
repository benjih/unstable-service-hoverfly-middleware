from unstable_service import UnstableService
import unittest
from unittest.mock import MagicMock
import os 
import json
import subprocess

class UnstableServiceTestSuite(unittest.TestCase):
	def setUp(self):
		self.unit = UnstableService()


	def test_read_stdin_and_return_result_as_dict(self):
		mock_stdin = MagicMock()
		mock_stdin.readlines.return_value = ['{"status": 200}']

		self.unit = UnstableService(stdin = mock_stdin)
		result = self.unit.read_stdin_and_create_dict()

		self.assertTrue(type(result) is dict)
		self.assertEqual(result['status'], 200)

	def test_random_sleep(self):
		mock_random = MagicMock()
		mock_random.randint.return_value = 57

		mock_sleep = MagicMock()

		result = self.unit = UnstableService(random = mock_random, sleep = mock_sleep)

		result = self.unit.random_sleep()

		self.assertEqual(result, 57)
		mock_random.randint.assert_called_with(0, 60)
		mock_sleep.assert_called_with(57)

	def test_incrementing_request_counter(self):
		f = open('request_counter', "w")
		f.write("0")
		f.close()

		result = self.unit.get_request_counter_and_update(10)

		self.assertFalse(result)

		f = open('request_counter')
		file_result = f.readline()
		f.close()

		self.assertEqual(file_result, "1")


	def test_resetting_request_counter(self):
		f = open('request_counter', "w")
		f.write("10")
		f.close()

		result = self.unit.get_request_counter_and_update(10)

		self.assertTrue(result)

		f = open('request_counter')
		file_result = f.readline()
		f.close()

		self.assertEqual(file_result, "0")

	def test_first_time_no_request_counter(self):
		os.remove(os.path.dirname(os.path.realpath(__file__)) + '/request_counter')

		result = self.unit.get_request_counter_and_update(10)

		self.assertEqual(False, result)

		f = open('request_counter')
		file_result = f.readline()
		f.close()

		self.assertEqual(file_result, "1")

	def test_convert_response_into_server_error(self):
		payload = {'response': {}}

		result = self.unit.convert_response_into_server_error(payload)

		self.assertTrue(type(result) is dict)
		self.assertEqual(result['response']['status'], 500)
		self.assertEqual(result['response']['body'], 'INTERNAL SERVER ERROR')


	def test_end_to_end_okay(self):
		ps = subprocess.Popen("""cat test_response | python unstable_service.py""", shell=True,
                     stdout=subprocess.PIPE,
                     stderr=subprocess.STDOUT)
		output,_= ps.communicate()

		result = json.loads(output.decode("ascii"))
		
		self.assertEqual(result['id'], "5d4f6b1d9f7c2407f78e4d4e211ec769")
		self.assertEqual(result['response']['status'], 200)

	def test_end_to_end_not_okay(self):
		f = open('request_counter', "w")
		f.write("10")
		f.close()

		ps = subprocess.Popen("""cat test_response | python unstable_service.py""", shell=True,
                     stdout=subprocess.PIPE,
                     stderr=subprocess.STDOUT)
		output,_= ps.communicate()

		result = json.loads(output.decode("ascii"))
		
		self.assertEqual(result['id'], "5d4f6b1d9f7c2407f78e4d4e211ec769")
		self.assertEqual(result['response']['status'], 500)

if __name__ == '__main__':
		unittest.main()
