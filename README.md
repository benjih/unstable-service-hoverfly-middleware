# Unstable Service
This is a piece of middleware intended to work as part of [Hoverfly by Specto Labs](https://github.com/SpectoLabs/hoverfly). The purpose 
of this piece of simulate the integration with a service that is unreliable.

This piece of middleware will randomly delay all incoming requests from a range of 0 to 60 seconds. It will also turn every nth response
into an error with a 500 response code.

## Dependencies:
 * Python 3.5
