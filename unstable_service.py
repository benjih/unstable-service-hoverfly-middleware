import logging
import json
import random
import sys
import time

logging.basicConfig(filename="unstable_service.log", level=logging.DEBUG)

FAILURE_RATE = 10

class UnstableService:
	def __init__(self, stdin = sys.stdin, stdout = sys.stdout, random = random, sleep = time.sleep):
		self.stdin, self.stdout, self.random, self.sleep = stdin, stdout, random, sleep

	def main(self):
		payload = self.read_stdin_and_create_dict()
		should_error = self.get_request_counter_and_update(FAILURE_RATE)

		sleep_duration = self.random_sleep()
		logging.debug("Delaying payload[%s] by %s seconds" % (payload['id'], sleep_duration))

		if should_error:
			logging.debug("Turning payload[%s] into an error" % payload['id'])
			payload = self.convert_response_into_server_error(payload)

		self.stdout.write(json.dumps(payload))

	def read_stdin_and_create_dict(self):
		data = self.stdin.readlines()
		logging.debug("0" + data[0])
		return json.loads(data[0])

	def random_sleep(self):
		sleep_duration = self.random.randint(0, 60)
		self.sleep(sleep_duration)
		return sleep_duration

	def get_request_counter_and_update(self, failure_rate):
		try:
			f = open('request_counter')
			request_counter = int(f.readline())
			f.close()
		except FileNotFoundError:
			request_counter = 0

		f = open('request_counter', 'w')
		if request_counter < failure_rate:
			f.write(str(request_counter + 1))
			f.close()
			return False
		else:
			f.write(str(0))
			f.close()
			return True

		return request_counter

	def convert_response_into_server_error(self, payload):
		payload['response']['status'] = 500
		payload['response']['body'] = 'INTERNAL SERVER ERROR'
		return payload

if __name__ == "__main__":
	unstable_service = UnstableService(stdin = sys.stdin, stdout = sys.stdout)
	unstable_service.main()
